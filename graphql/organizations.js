const models = require('../models')

exports.schema = /* GraphQL */ `
type Organization {
  id: ID!
  name: String!
  users: [User]!
}

extend type Query {
  organizations__current_user: [Organization]!
  organizations__find(id: ID!): Organization
}

extend type Mutation {
  organizations__create(name: String!): Organization!
  organizations__update(id: ID!, name: String!): Organization!
}
`;
exports.resolvers = {
  organizations__current_user: (_, { auth }) => {
    if (!auth) {
      throw new Error('unauthenticated')
    }
    return auth.getOrganizations()
  },

  organizations__find: async ({ id }, { fieldsList }, info) => {
    const organization = await models.Organization.findByPk(id)
    if (organization && fieldsList(info).includes('users')) {
      organization.users = organization.getUsers()
    }
    return organization;
  },

  organizations__create: async ({ name }, { auth, models }) => {
    if (!auth) {
      throw new Error('you must log in to create an organization')
    }
    const organization = await auth.createOrganization({ name })
    models.User.update({ OrganizationId: organization.id }, { where: { id: auth.id }})
    return organization
  },

  organizations__update: async ({ id, name }) => {
    await models.Organization.update({ name }, {
      where: {
        id,
      }
    })
    return models.User.findByPk(id)
  },
  
}
