const user = require("../models/user");

exports.schema = /* GraphQL */ `
type User {
  id: ID!
  name: String!
  email: String!
  bearer_token: String
  organizations: [Organization]!
}

extend type Query {
  users__all: [User!]!
  users__current: User!
  users__find(id: ID!): User
}

extend type Mutation {
  users__register(name: String!, email: String!, password: String!): User!
  users__update(id: ID!, name: String, email: String, password: String): User!
  users__set_organization(OrganizationId: ID!): Organization!
  users__login(email: String!, password: String!): User!
  users__logout(id: ID): User!
}
`;

exports.resolvers = {

  users__all: async (_, { fieldsList }, info) => {
    const users = await  models.User.findAll();
    return users.map(user => {
      if (fieldsList(info).includes('organizations')) {
        user.organizations = user.getOrganizations()
      }
      return user
    })
  },

  users__current: (_, { auth, fieldsList }, info) => {
    const fields = fieldsList(info)
    if (auth && fields.includes('organizations')) {
      auth.organizations = auth.getOrganizations();
    }
    return auth
  },

  users__find: async ({ id }, { fieldsList, models, auth }, info) => {
    const fields = fieldsList(info)
    const user = await models.User.findByPk(id)
    if (!await auth.can.viewUser(user)) {
      throw new Error('canot view user')
    }
    if (user && fields.includes('organizations')) {
      user.organizations = user.getOrganizations();
    }
    return user;
  },

  users__register: ({ name, email, password }, { validator, models }) => {
    if (!validator.isEmail(email)) {
      throw new Error('invalid email')
    }
    const { User } = models
    password = User.createPassword(password)
    const bearer_token = User.generateBearerToken()
    return User.create({ name, email, password, bearer_token })
  },

  users__update: async ({ id, name, email }, { models }) => {
    const dto = {}
    if (name) {
      dto.name = name;
    }
    if (email) {
      dto.email = email;
    }
    await models.User.update(dto, {
      where: {
        id,
      }
    })
    return models.User.findOne({ id })
  },

  users__set_organization: async ({ OrganizationId }, { models, auth }) => {
    const organization = await models.Organization.findByPk(OrganizationId)
    if (!organization) {
      throw new Error('organization not found')
    }
    if (!await auth.can.useOrganization(organization)) {
      throw new Error('you are not part of this organization')
    }
    models.User.update({ OrganizationId }, { where: { id: auth.id }})
    return organization
  },
  
  users__login: async ({ email, password}, { models, validator }) => {
    if (!validator.isEmail(email)) {
      throw new Error('invalid email')
    }
    const user = await models.User.findOne({
      where: {
        email,
      }
    })
    if (!user) {
      throw new Error('user does not exist')
    }
    if (!user.validatePassword(password)) {
      throw new Error('invalid password')
    }
    if (!user.bearer_token) {
      user.bearer_token = models.User.generateBearerToken()
      models.User.update({ bearer_token: user.bearer_token }, {where: { id: user.id }})
    }
    return user
  },

  users__logout: async ({ id }, { models, user }) => {
    models.User.update({ bearer_token: null }, {
      where: { id }
    })
    return await models.User.findByPk(id)
  }

}
