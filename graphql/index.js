const users = require('./users');
const organizations = require('./organizations')

exports.resolvers = {
  ...users.resolvers,
  ...organizations.resolvers
}

const { buildSchema } = require('graphql')
const baseSchema = /* GraphQL */`
type Query
type Mutation
`;
exports.types = buildSchema(
  baseSchema +
  users.schema +
  organizations.schema
)

