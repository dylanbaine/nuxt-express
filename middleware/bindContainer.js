const { fieldsList } = require('graphql-fields-list')
const models = require('../models')
const validator = require('validator')

exports.bindContainer = async function(req, _, next) {
  req.auth = false
  const bearer_token = (req.headers.authorization || false) ? req.headers.authorization.replace('Bearer', '').trim() : false
  if (bearer_token) {
    req.auth = await models.User.findOne({ where: { bearer_token } })
    if (req.auth === null) {
      throw new Error('login plz')
    }
  }
  req.fieldsList = fieldsList
  req.models = models
  req.validator = validator
  next()
}
