'use strict'
const crypto = require('crypto')
const {
  Model,
  Sequelize
} = require('sequelize')
const { userPolicy } = require('../policies/userPoliciy')

module.exports = (sequelize, DataTypes) => {
  class User extends Model {

    validatePassword(rawPassword) {
      return this.password === User.createPassword(rawPassword)
    }
  
    static createPassword(password) {
      const sha256 = crypto.createHash('sha256')
      const hash = sha256.update(password).digest('base64')
      return hash
    }

    static generateBearerToken() {
      return crypto.randomBytes(30).toString('hex')
    }

    static associate(models) {
      models.Organization.belongsToMany(models.User, {
        through: 'OrganizationUsers'
      })
      models.User.belongsTo(models.Organization)
    }

    get can() {
      return userPolicy(this)
    }

  }
  User.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    name: {
      type: Sequelize.STRING
    },
    bearer_token: {
      type: Sequelize.STRING
    },
    OrganizationId: {
      type: Sequelize.INTEGER,
    },
    password: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE
    }
  }, {
    sequelize,
    modelName: 'User',
  })
  return User
}