exports.userPolicy = function(user) {
  return {
    async viewUser(_user) {
      if (user.id == _user.id) return true
      const org = await user.getOrganization()
      if (org) {
        return (await org.getUsers()).map(u => u.id).includes(_user.id)
      }
      return false;
    },

    async useOrganization(organization) {
      return (await organization.getUsers()).map(u => u.id).includes(user.id)
    }

  }
}