require('dotenv').config()

const { loadNuxt, build } = require('nuxt')

const app = require('express')()
const isDev = process.env.NODE_ENV !== 'production'
const port = process.env.PORT || 3000
const { graphqlHTTP } = require('express-graphql');
const Schema = require('./graphql')
const { bindContainer } = require('./middleware/bindContainer')
const { sequelize } = require('./models')

if (isDev) {
  sequelize.sync()
}

async function start() {
  // We get Nuxt instance
  const nuxt = await loadNuxt(isDev ? 'dev' : 'start')

  app.use(bindContainer)

  const handleDevError = (error) => ({
    message: error.message,
    locations: error.locations,
    stack: error.stack ? error.stack.split('\n') : [],
    path: error.path,
  });
  const handleProdError = (error) => {
    throw new Error(error.message)
  }
  const graphServer = graphqlHTTP({
    schema: Schema.types,
    rootValue: Schema.resolvers,
    graphiql: isDev,
    customFormatErrorFn: isDev ? handleDevError : handleProdError,
  });
  app.use('/graphql', graphServer)
    
  app.use(nuxt.render)

  // Build only in dev mode with hot-reloading
  if (isDev) {
    build(nuxt)
  }

  // Listen the server
  app.listen(port, '0.0.0.0')
  console.log('Server listening on `localhost:' + port + '`.')
}

start()
